/*******************************************************************************
 Data Stream usb Source File

  File Name:
    usb.c

  Summary:
 Data Stream USART source

  Description:
    This file contains source code necessary for the data stream interface.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

#include "bootloader/src/encbootloader.h"
#include "../nvm.h"
#include "peripheral/nvm/plib_nvm.h"
#include "system/common/sys_module.h"
#include "bootloader/src/datastream.h"
//#include "peripheral/ports/plib_ports.h"
#include "peripheral/dma/plib_dma.h"
#include "system/clk/sys_clk.h"
#include "usb/usb_host.h"
#include "usb/usb_host_msd.h"
#include "usb/usb_host_scsi.h"
#include "system/int/sys_int.h"
#include "system/tmr/sys_tmr.h"
#include "FileStructs.h"
#include "GenericTypeDefs.h"
#include "CRC.h"
#include "crypto/crypto.h"
#include "Aes.h"
#include "driver/sqi_flash/sst26/drv_sst26.h"

size_t FLASH_FS_FileRead(SYS_FS_HANDLE handle, void *buffer, size_t nbyte);
extern void SYS_MinTasks(void);


typedef enum {
    REC_FLASHED = 0,
    REC_NOT_FOUND,
    REC_FOUND_BUT_NOT_FLASHED
} T_REC_STATUS;

typedef struct
{
    uint8_t *start;
    uint8_t len;
    T_REC_STATUS status;
}T_REC;

typedef struct
{
	uint8_t RecDataLen;
	uint32_t Address;
	uint8_t RecType;
	uint8_t* Data;
	uint8_t CheckSum;
	uint32_t ExtSegAddress;
	uint32_t ExtLinAddress;
}T_HEX_RECORD;

T_REC record;

extern BOOTLOADER_DATA bootloaderData __attribute__((coherent, aligned(16)));

static uint8_t readBuffer[512] __attribute__((coherent, aligned(16)));
static uint8_t writeBuffer[4096] __attribute__((coherent, aligned(16)));
static _FlashFrame Enc_Frame __attribute__((coherent, aligned(16)));

USB_HOST_EVENT_RESPONSE Bootloader_BufferEventHandler(USB_HOST_EVENT event, void * eventData, uintptr_t context);

static unsigned int DiskType = 0;
static unsigned long FilePtr = 0;

/********************************************************************
* Function:     ConvertAsciiToHex()
*
* Precondition:
*
* Input:        ASCII buffer and hex buffer.
*
* Output:
*
* Side Effects: No return from here.
*
* Overview:     Converts ASCII to Hex.
*
*
* Note:         None.
********************************************************************/

void ConvertAsciiToHex(uint8_t* asciiRec, uint8_t* hexRec)
{
    uint8_t i = 0;
    uint8_t k = 0;
    uint8_t hex;


    while((asciiRec[i] >= 0x30) && (asciiRec[i] <= 0x66))
    {
            // Check if the ASCII values are in alpha numeric range.

            if(asciiRec[i] < 0x3A)
            {
                    // Numerical representation in ASCII found.
                    hex = asciiRec[i] & 0x0F;
            }
            else
            {
                    // Alphabetical value.
                    hex = 0x09 + (asciiRec[i] & 0x0F);
            }

            // Following logic converts 2 bytes of ASCII to 1 byte of hex.
            k = i%2;

            if(k)
            {
                    hexRec[i>>1] |= hex;

            }
            else
            {
                    hexRec[i>>1] = (hex << 4) & 0xF0;
            }
            i++;
    }

}

void Bootloader_ProcessBuffer( BOOTLOADER_DATA *handle )
{
    uint32_t i;
    uint32_t recCount = 0;
    size_t numBytes = handle->bufferSize;

    for(i = 0; i < (numBytes + handle->cmdBufferLength); i ++)
    {
            // This state machine seperates-out the valid hex records from the read 512 bytes.
        switch(record.status)
        {
            case REC_FLASHED:
            case REC_NOT_FOUND:
                if(handle->data->buffer[i] == ':')
                {
                        // We have a record found in the 512 bytes of data in the buffer.
                    record.start = &handle->data->buffer[i];
                    record.len = 0;
                    record.status = REC_FOUND_BUT_NOT_FLASHED;
                }
                break;
            case REC_FOUND_BUT_NOT_FLASHED:
                if((handle->data->buffer[i] == 0x0A) || (handle->data->buffer[i] == 0xFF))
                {
                        // We have got a complete record. (0x0A is new line feed and 0xFF is End of file)
                    // Start the hex conversion from element
                    // 1. This will discard the ':' which is
                    // the start of the hex record.
                    ConvertAsciiToHex(&record.start[1],&record.start[1]);
                    APP_ProgramHexRecord(&record.start[1], record.len >> 1);
                    recCount++;
                    record.status = REC_FLASHED;
                }
                break;
        }
        // Move to next byte in the buffer.
        record.len ++;
    }

    if(record.status == REC_FOUND_BUT_NOT_FLASHED)
    {
            // We still have a half read record in the buffer. The next half part of the record is read
            // when we read 512 bytes of data from the next file read.
        memcpy(handle->data->buffer, record.start, record.len);
        handle->cmdBufferLength = record.len;
        record.status = REC_NOT_FOUND;
    }
    else
    {
        handle->cmdBufferLength = 0;
    }
}

BOOL ProgramHexRecord(_FlashBlock * record) {
    DWORD* pData;
    DWORD_VAL totalAddress;

    //Generic loop index.  Needs to be a WORD since a record could be 255 bytes
    //  and in this case written 4 bytes at a time.  Thus the counts for that loop
    //  would be 252 and 256.  Since 256 can't be represented in a byte, a loop
    //  counter of a byte could only count up to 252 bytes.
    WORD i;
    if (record->PageInfo.BlockLength == 0) {
	return TRUE;
    }
    totalAddress.Val = record->PageInfo.Address;

    pData = (DWORD*) record->Frame;

    for (i = 0; i < record->PageInfo.BlockLength; i += 4) {
        //Program the data
        APP_NVMWordWrite((void *) totalAddress.Val, *pData++);

        totalAddress.Val += 4;
    }

    //verify that the contents were programmed correctly
    pData = (DWORD*) record->Frame;
    totalAddress.Val -= (record->PageInfo.BlockLength);

    for (i = 0; i < record->PageInfo.BlockLength; i += 4) {
        if (*((DWORD *) PA_TO_KVA1(totalAddress.Val)) != *pData++) {
            //data in flash doesn't match expected value,
            //  close file and bail.
            //BLIO_ReportBootStatus(LOADER_FLASH_VERIFY_ERR, "BL: Data in flash doesn't match expected value.\r\n" );
            return FALSE;
        }

        totalAddress.Val += 4;
    }

    //Everything programmed correctly
    return TRUE;
}

BOOL ProgramExternalRecord(_FlashBlock * record) {
    DWORD* pData;
    DWORD* pBuff;
    volatile unsigned long totalAddress;
    DRV_SST26_COMMAND_STATUS cmdStatus;
    unsigned long i = 0;
    unsigned long a;
    int task = 0;
    int loop = 1;
    unsigned long Blocks = record->PageInfo.BlockLength / 0x100;
    if (record->PageInfo.BlockLength == 0) {
	return TRUE;
    } else if (Blocks == 0) {
	Blocks = 1; //We have to write a minimum block
    }
    totalAddress = record->PageInfo.Address - FLASH_USER_BASE;
    pData = (DWORD*) record->Frame;
    DRV_SST26_Write(bootloaderData.streamHandle, &bootloaderData.commandHandle, (void*) pData, totalAddress / 0x100, Blocks);
    if (DRV_SST26_COMMAND_HANDLE_INVALID == bootloaderData.commandHandle) {
	PrintUart("Flash write error at 0x%X\r\n", totalAddress);
	return FALSE;
    }
    while (1) {
	cmdStatus = DRV_SST26_CommandStatus(bootloaderData.streamHandle, bootloaderData.commandHandle);
	if (cmdStatus == DRV_SST26_COMMAND_COMPLETED) {
	    break;
	} else if (DRV_SST26_COMMAND_ERROR_UNKNOWN == cmdStatus) {
	    PrintUart("Unable to program flash at 0x%X\r\n", totalAddress);
	    return FALSE;
	}
	SYS_MinTasks();
    }

    //verify that the contents were programmed correctly
    //pData = (DWORD*) record->Frame;
    //totalAddress -= (record->PageInfo.BlockLength);
    task = i = 0;
    loop = 1;
    while (loop) {
	SYS_MinTasks();
	switch (task) {
	    case 0:
		DRV_SST26_Read(
			bootloaderData.streamHandle, &bootloaderData.commandHandle,
			(void*) readBuffer,
			totalAddress,
			0x100);
		if (DRV_SST26_COMMAND_HANDLE_INVALID == bootloaderData.commandHandle) {
		    PrintUart("External flash read error at 0x%X\r\n", totalAddress);
		    return FALSE;
		}
		task = 1;
		i += 0x100;
		break;
	    case 1:
		cmdStatus = DRV_SST26_CommandStatus(bootloaderData.streamHandle, bootloaderData.commandHandle);
		if (cmdStatus == DRV_SST26_COMMAND_COMPLETED) {
		    pBuff = (DWORD*) readBuffer;
		    for (a = 0; a < 0x100 / 4; a++) {
			if (*pBuff != *pData) {
			    PrintUart("External flash verify error at 0x%X\r\n", totalAddress);
			    return FALSE;
			}
			pBuff++;
			pData++;
			totalAddress += 4;
		    }
		    if (i >= record->PageInfo.BlockLength) {
			loop = 0;
			break;
		    } else {
			task = 0;
		    }
		} else if (DRV_SST26_COMMAND_ERROR_UNKNOWN == cmdStatus) {
		    PrintUart("Unable to read external flash at 0x%X\r\n", totalAddress);
		    return FALSE;
		}
		break;
	}
    }
    //Everything programmed correctly
    return TRUE;
}

static int BootloaderState = 0;
enum {
    BL_Init, BL_ReadStartIV, BL_Program, BL_validate , BL_Error
};

extern void MyCallBack(int i, int v);

int Bootloader_ProcessFile(int Validate) {
#define Aes_Soft 1
    static unsigned int nRemaining; // Number of bytes remaining to decode
    static unsigned long AddressMarker, ProgramLength = 0, TempCRC = 0, LastBlock = 0, LastID = 0, LastLength = 0;
    static unsigned long ExternProgramLength = 0, ExternTempCRC = 0;
    static _PageHeader Enc_Header;
    static _FlashFrame Enc_Frame;
#ifndef Aes_Soft
    static CRYPT_AES_CTX ctx;
#else
    static AES_CTX ctx;
#endif
    static WORD i, a;
    static _FileHeader FH;
    static BOOL ProgExternFlash = FALSE;
    int ret;
    size_t(*Transport_read)(SYS_FS_HANDLE handle, void *buffer, size_t nbyte);
    if (DiskType == Media_USB) {
	Transport_read = SYS_FS_FileRead;
    } else {
	Transport_read = FLASH_FS_FileRead;
    }
    
    switch (BootloaderState) {
	case BL_Init:
	    AddressMarker = ProgramLength = TempCRC = LastBlock = 
		    LastID = LastLength = ExternProgramLength = ExternTempCRC = nRemaining = 0;
	    BootloaderState = BL_ReadStartIV;
	    memset(&ctx, 0, sizeof (ctx));
	    break;
	case BL_ReadStartIV:
	    nRemaining = Transport_read(bootloaderData.fileHandle, (void *) &FH, sizeof (FH));
	    if (crc32(FH.IV, 16, 0xFFFFFFFF) != FH.CRC || nRemaining == 0) {
		PrintUart("BL: Filer error 1\r\n");
		return BLT_INVALID;
	    }
#ifndef Aes_Soft
	    ret = CRYPT_AES_KeySet(&ctx, Key128, 16, FH.IV, CRYPT_AES_DECRYPTION);
	    if (ret) {
		PrintUart("BL: Aes issue\r\n");
		return BLT_INVALID;
	    }
#else
	    AES_generateSBox();
	    AES_set_key(&ctx, Key128, FH.IV, AES_MODE_128);
	    AES_convert_key(&ctx);
#endif	    
	    RunningCRC32Seed(0xFFFFFFFF);
	    AddressMarker = PROGRAM_FLASH_BASE;
	    i = 0;
	    BootloaderState = BL_Program;
	    ProgExternFlash = FALSE;
	    break;
	case BL_Program:
	    MyCallBack(i, Validate);
	    nRemaining = Transport_read(bootloaderData.fileHandle, (void *) &Enc_Header, sizeof (Enc_Header));
	    if (nRemaining == 0 || nRemaining < sizeof (Enc_Header)) {
		PrintUart("BL: File error 2\r\n");
		return BLT_INVALID;
	    }
#ifndef Aes_Soft
	    ret = CRYPT_AES_CBC_Decrypt(&ctx, (unsigned char*) &Dec_Block.PageInfo, (unsigned char*) &Enc_Header, sizeof ( Enc_Header));
	    if (ret) {
		PrintUart("BL: Aes issue @ BL_Program\r\n");
		return BLT_INVALID;
	    }
#else
	    AES_cbc_decrypt(&ctx, (unsigned char*) &Enc_Header, (unsigned char*) &Dec_Block.PageInfo, sizeof ( Enc_Header));
#endif
	    if (Dec_Block.PageInfo.BlockLength < 0x1000 && Dec_Block.PageInfo.BlockLength >= 0) {
		//Last row
		if (ProgExternFlash) {
		    ExternProgramLength = Dec_Block.PageInfo.Address - FLASH_USER_BASE + Dec_Block.PageInfo.BlockLength;
		    ExternTempCRC = Dec_Block.PageInfo.FlashChecksum;
		} else {
		    ProgramLength = Dec_Block.PageInfo.Address - PROGRAM_FLASH_BASE + Dec_Block.PageInfo.BlockLength;
		    TempCRC = Dec_Block.PageInfo.FlashChecksum;
		}
		LastBlock = TRUE;
		LastID = Dec_Block.PageInfo.BlockLength / 256;
		LastLength = Dec_Block.PageInfo.BlockLength % 256;
	    }
	    if (Dec_Block.PageInfo.Address != AddressMarker) {
		PrintUart("BL: Address out of sequence. %X != %X\r\n", Dec_Block.PageInfo.Address, AddressMarker);
		return BLT_INVALID;
	    }
	    for (a = 0; a < 16; a++) {
		nRemaining = Transport_read(bootloaderData.fileHandle, (void *) &Enc_Frame, sizeof (Enc_Frame));
		if (nRemaining == 0 || nRemaining < sizeof (Enc_Frame)) {
		    PrintUart("BL: File error 3\r\n");
		    return BLT_INVALID;
		}
#ifndef Aes_Soft
		ret = CRYPT_AES_CBC_Decrypt(&ctx, (unsigned char*) &Dec_Block.Frame[a], (unsigned char*) &Enc_Frame, sizeof ( Enc_Frame));
		if (ret) {
		    PrintUart("BL: Aes issue @ BL_Program %i\r\n", a);
		    return BLT_INVALID;
		}
#else
		AES_cbc_decrypt(&ctx, (unsigned char*) &Enc_Frame, (unsigned char*) &Dec_Block.Frame[a], sizeof ( Enc_Frame));
#endif
		if (LastBlock && LastID == a) {
		    //very last frame
		    if (RunningCRC32(&Dec_Block.Frame[a], 0x100) != Dec_Block.PageInfo.FrameCRC[a]) {
			PrintUart("BL: File error 4\r\n");
			return BLT_INVALID;
		    }
		    break;
		} else {
		    unsigned long RCRC = RunningCRC32(&Dec_Block.Frame[a], 0x100);
		    if (RCRC != Dec_Block.PageInfo.FrameCRC[a]) {
			PrintUart("BL: File error 5\r\n");
			return BLT_INVALID;
		    }
		}
		AddressMarker += 0x100;
	    }
	    if (!Validate) {
		if (ProgExternFlash) {
		    if (ProgramExternalRecord(&Dec_Block) == FALSE) {
			PrintUart("BL: Program external flash block err %i\r\n", i);
			return BLT_INVALID;
		    } else {
			PrintUart("\rBL: Programmed external flash block %i     ", i);
		    }
		} else {
		    if (ProgramHexRecord(&Dec_Block) == FALSE) {
			PrintUart("BL: Program flash block err %i\r\n", i);
			return BLT_INVALID;
		    } else {
			PrintUart("\rBL: Programmed flash block %i     ", i);
		    }
		}
	    }
	    if (LastBlock && !ProgExternFlash) {
		if (!Validate) {
		    PrintUart("\r\nBL: Flash programmed\r\n", i);
		}
		ProgExternFlash = TRUE;
		LastBlock = FALSE;
		RunningCRC32Seed(0xFFFFFFFF);
		AddressMarker = FLASH_USER_BASE;
		AES_set_key(&ctx, Key128, FH.IV, AES_MODE_128);
		AES_convert_key(&ctx);
		i = FlashLength;
		//Jump to flash data
		unsigned long seekpoint = sizeof(_FileHeader) + (FlashLength * sizeof(_FlashBlock));
		if (DiskType == Media_USB) {
		    int s = SYS_FS_FileSeek(bootloaderData.fileHandle, seekpoint, SYS_FS_SEEK_SET);
		    if (s != seekpoint) {
			PrintUart("BL: Unable to seek set to %i was %i\r\n", seekpoint, s);
			return BLT_INVALID;
		    }
		} else {
		    FilePtr = seekpoint;
		}

	    }
	    if (LastBlock && ProgExternFlash) {
		BootloaderState = BL_validate;
	    }
	    i++;
	    if (i > FlashLength + ExternalFlashLength) {
		PrintUart("BL: File too long\r\n");
		return BLT_INVALID;
	    }
	    break;
	case BL_validate:
	    if (!Validate) {
		if (TempCRC == crc32(PA_TO_KVA1(PROGRAM_FLASH_BASE), ProgramLength, 0xFFFFFFFF)) {
		    BLMedia_SaveKey();
		    PrintUart("\r\nBL: Programmed and validated\r\n");
		    return BLT_SUCCESS;
		} else {
		    return BLT_INVALID;
		}
	    } else {
		return BLT_SUCCESS;
	    }
	    break;
	case BL_Error:
	    return BLT_INVALID;
    }
    return BLT_UNFINISHED;
}

//Reset state machine for above

void Bootloader_Init(void) {
    if (DiskType == Media_USB) {
	SYS_FS_FileSeek(bootloaderData.fileHandle, 0, SYS_FS_SEEK_SET);
    }
    BootloaderState = BL_Init;
    FilePtr = 0;
}

void Bootloader_SetDisk(int Disk_Type) {
    DiskType = Disk_Type;
}

/*******************************************************
 * USB HOST MSD Layer Events - Application Event Handler
 *******************************************************/
void Bootloader_USBHostMSDEventHandler(SYS_FS_EVENT event, void * eventData, uintptr_t context) {

    const char * mountName = (const char *) eventData;
    if (DiskType == Media_USB && 0 == strcmp((const char *) mountName, "/mnt/myUsbDrive")) {
	switch (event) {
	    case SYS_FS_EVENT_MOUNT:
		bootloaderData.currentState = BOOTLOADER_DEVICE_CONNECTED;
		break;
	    case SYS_FS_EVENT_UNMOUNT:
		bootloaderData.currentState = BOOTLOADER_UNMOUNT_DISK;
		break;
	    case SYS_FS_EVENT_ERROR:
		bootloaderData.currentState = BOOTLOADER_ERROR;
		break;
	    default:
		break;
	}
    }
}


/*******************************************************
 * USB HOST Layer Events - Host Event Handler
 *******************************************************/
USB_HOST_EVENT_RESPONSE Bootloader_BufferEventHandler (USB_HOST_EVENT event, void * eventData, uintptr_t context)
{
    return USB_HOST_EVENT_RESPONSE_NONE;
}

void DATASTREAM_BufferEventHandlerSet
(
    const DRV_HANDLE hClient,
    const void * eventHandler,
    const uintptr_t context
)
{
    /* This means host operation is enabled. We can
     * move on to the next state */
	SYS_FS_EventHandlerSet(Bootloader_USBHostMSDEventHandler, (uintptr_t) NULL);
	USB_HOST_EventHandlerSet(Bootloader_BufferEventHandler, 0);

}

DRV_HANDLE DATASTREAM_Open(const DRV_IO_INTENT ioIntent) {
    if (DiskType == Media_USB) {
	//USB_HOST_BusEnable(0);
    }
    return DRV_SST26_Open(DRV_SST26_INDEX_0, DRV_IO_INTENT_READWRITE);
}

DRV_CLIENT_STATUS DATASTREAM_ClientStatus(DRV_HANDLE handle) {
    if (DiskType == Media_USB) {
	if (USB_HOST_BusIsEnabled(0) && DRV_SST26_IsAttached(handle)) {
	    return DRV_CLIENT_STATUS_READY;
	} else {
	    return DRV_CLIENT_STATUS_BUSY;
	}
    } else {
	if (DRV_SST26_IsAttached(handle)) {
	    return DRV_CLIENT_STATUS_READY;
	} else {
	    return DRV_CLIENT_STATUS_BUSY;
	}
    }
}

size_t FLASH_FS_FileRead(SYS_FS_HANDLE handle, void *buffer, size_t nbyte) {
    int task = 0;
    DRV_SST26_COMMAND_STATUS cmdStatus;
    while (1) {
	SYS_MinTasks();
	switch (task) {
	    case 0:
		DRV_SST26_Read(bootloaderData.streamHandle, &bootloaderData.commandHandle,
			buffer, FLASH_FIRMWARE + FilePtr, nbyte);
		if (DRV_SST26_COMMAND_HANDLE_INVALID == bootloaderData.streamHandle) {
		    PrintUart("Flash IO error 1\r\n");
		    return -1;
		}
		task = 1;
	    case 1:
		cmdStatus = DRV_SST26_CommandStatus(bootloaderData.streamHandle, bootloaderData.commandHandle);
		if (cmdStatus == DRV_SST26_COMMAND_COMPLETED) {
		    FilePtr += nbyte;
		    return nbyte;
		} else if (DRV_SST26_COMMAND_ERROR_UNKNOWN == cmdStatus) {
		    PrintUart("Flash IO error 2\r\n");
		    return -1;
		}
		break;
	}
    }
}

int DATASTREAM_Data_Read(uintptr_t * const bufferHandle, unsigned char* buffer, const int maxsize)
{
    return(SYS_FS_FileRead( bootloaderData.fileHandle, (void *)buffer, maxsize ));
}

int DATASTREAM_Data_Write(uintptr_t * const bufferHandle, unsigned char* buffer, const int bufsize)
{
   //No writing performed for USB Host
    return(0);
}

void DATASTREAM_Close(void) {
    if (bootloaderData.streamHandle != DRV_HANDLE_INVALID) {
	DRV_SST26_Close(bootloaderData.streamHandle);
    }
    if (DiskType == Media_USB && bootloaderData.fileHandle != DRV_HANDLE_INVALID) {
	SYS_FS_FileClose(bootloaderData.fileHandle);
    }
    //Disable Interrupt sources so bootloader application runs without issues
    SYS_INT_SourceDisable(DRV_TMR_INTERRUPT_SOURCE_IDX0);
    SYS_INT_VectorPrioritySet(DRV_TMR_INTERRUPT_VECTOR_IDX0, INT_DISABLE_INTERRUPT);
    SYS_INT_VectorSubprioritySet(DRV_TMR_INTERRUPT_VECTOR_IDX0, INT_SUBPRIORITY_LEVEL0);
    SYS_INT_SourceDisable(INT_SOURCE_USB_1);
    SYS_INT_VectorPrioritySet(INT_VECTOR_USB1, INT_DISABLE_INTERRUPT);
    SYS_INT_VectorSubprioritySet(INT_VECTOR_USB1, INT_SUBPRIORITY_LEVEL0);
#if defined(_USB_DMA_VECTOR)
    SYS_INT_SourceDisable(INT_SOURCE_USB_1_DMA);
    SYS_INT_VectorPrioritySet(INT_VECTOR_USB1_DMA, INT_DISABLE_INTERRUPT);
    SYS_INT_VectorSubprioritySet(INT_VECTOR_USB1_DMA, INT_SUBPRIORITY_LEVEL0);
#endif
        
    PLIB_TMR_Stop(DRV_TMR_PERIPHERAL_ID_IDX0);
}
