/*******************************************************************************
  Company:
    Microchip Technology Inc.

  File Name:
    bootloader.c
    
  Summary:
    Interface for the Bootloader library.

  Description:
    This file contains the interface definition for the Bootloader library.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Include Files
// *****************************************************************************
// *****************************************************************************
#include <sys/attribs.h>
#include <sys/kmem.h>
#include "uart.h"
#include "app.h"
#include "bootloader/src/encbootloader.h"
#include "peripheral/nvm/plib_nvm.h"
#include "peripheral/int/plib_int.h"
#include "system/devcon/sys_devcon.h"
#include "system/reset/sys_reset.h"
#include "uart.h"
#include "FileStructs.h"
#include "global.h"

ApplicationVerification volatile __attribute__ ((space(prog),section (".bootloader_mem"))) g_AppVerify =
{
    0, 0,0,0
};

#if(SYS_FS_MAX_FILES > 0)
uint8_t fileBuffer[512] __attribute__((coherent, aligned(16)));
uint8_t longfilename[300];
#endif

BOOTLOADER_DATA bootloaderData __attribute__((coherent, aligned(16)));
BOOTLOADER_BUFFER data_buff __attribute__((coherent, aligned(16)));

void Bootloader_BufferEventHandler(DATASTREAM_BUFFER_EVENT buffEvent,
                            DATASTREAM_BUFFER_HANDLE hBufferEvent,
                            uint16_t context );

extern void APP_NVMQuadWordWrite(void* address, uint32_t* data);

/********************************************************************
* Function:     Enter_Application()
*
* Precondition:
*
* Input:        None.
*
* Output:
*
* Side Effects: No return from here.
*
* Overview:     The function will program the checksum for the respective
 *              flash panel and jumps to application programmed in it.
*
*
* Note:
********************************************************************/
static void Enter_Application(void)
{
    void (*fptr)(void);

    /* Set default to APP_RESET_ADDRESS */
    fptr = (void (*)(void))APP_RESET_ADDRESS;
    /* Disable Global Interrupts and Jump to Application*/
    PLIB_INT_Disable(INT_ID_0);
    if (bootloaderData.StartAppFunc != NULL) {
	bootloaderData.StartAppFunc();
    }
    fptr();
}

/**
 * Static table used for the table_driven implementation.
 *****************************************************************************/
static const uint16_t crc_table[16] =
{
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef
};

/********************************************************************
* Function:     CalculateCrc()
*
* Precondition:
*
* Input:        Data pointer and data length
*
* Output:       CRC.
*
* Side Effects: None.
*
* Overview:     Calculates CRC for the given data and len
*
*
* Note:         None.
********************************************************************/
uint32_t APP_CalculateCrc(uint8_t *data, uint32_t len)
{
    uint32_t i;
    uint16_t crc = 0;
    
    while(len--)
    {
        i = (crc >> 12) ^ (*data >> 4);
        crc = crc_table[i & 0x0F] ^ (crc << 4);
        i = (crc >> 12) ^ (*data >> 0);
        crc = crc_table[i & 0x0F] ^ (crc << 4);
        data++;
    }

    return (crc & 0xFFFF);
}
/******************************************************************************
  Function:
    SYS_MODULE_OBJ Bootloader_Initialize(const SYS_MODULE_INDEX   moduleIndex,
                              const SYS_MODULE_INIT    * const moduleInit)

  Summary:
    Initializes primitive data structures for the general features
    of the primitive layer.

  Description:
    Initializes external and internal data structure for the general
    features of the primitive layer.

    This function must be called at system initialization.

  Remarks:
    None.
*/
void Bootloader_Initialize ( const BOOTLOADER_INIT *drvBootloaderInit )
{
    /* Place the App state machine in it's initial state. */
    bootloaderData.currentState = BOOTLOADER_CHECK_FSDEV;
    bootloaderData.cmdBufferLength = 0;
    bootloaderData.streamHandle = DRV_HANDLE_INVALID;
    bootloaderData.datastreamStatus = DRV_CLIENT_STATUS_ERROR;
    bootloaderData.usrBufferEventComplete = false;

    bootloaderData.data = &data_buff;
    bootloaderData.type = drvBootloaderInit->drvType;
    bootloaderData.FlashEraseFunc = (BOOTLOADER_CALLBACK)NULL;
    bootloaderData.StartAppFunc = (BOOTLOADER_CALLBACK)NULL;
    bootloaderData.BlankCheckFunc = (BOOTLOADER_CALLBACK)NULL;
    bootloaderData.ProgramCompleteFunc = (BOOTLOADER_CALLBACK)NULL;
    bootloaderData.ForceBootloadFunc = (BOOTLOADER_CALLBACK)NULL;
    bootloaderData.ResetCause = Reset_POR;
    if (RCONbits.BOR && !RCONbits.POR) {

    } else if (RCONbits.POR) {
	//Probably just a normal start
    } else if (RCONbits.EXTR) {
	//bootloaderData.ResetCause = Reset_MCLR;
	//MCLR, USB bootload mode
	//ModeSwitch = Mode_USB;
    } else if (RCONbits.SWR) {
	bootloaderData.ResetCause = Reset_SWR;
    } else if (RCONbits.WDTO) {
	bootloaderData.ResetCause = Reset_WDT;
    }
    RCONCLR = ~_RCON_WDTO_MASK;//Leave RCONbits.WDTO for main app to see
    bootloaderData.softReset = (SYS_RESET_ReasonGet() & RESET_REASON_SOFTWARE) == RESET_REASON_SOFTWARE;
    SYS_RESET_ReasonClear(RESET_REASON_SOFTWARE);
    /* Delay to allow the internal pullups to stabilize */
    _CP0_SET_COUNT(0);
    while (_CP0_GET_COUNT() < SYS_CLK_FREQ / 5000);
}

void BOOTLOADER_FlashEraseRegister(BOOTLOADER_CALLBACK newFunc)
{
    bootloaderData.FlashEraseFunc = newFunc;
}

void BOOTLOADER_StartAppRegister(BOOTLOADER_CALLBACK newFunc)
{
    bootloaderData.StartAppFunc = newFunc;
}

void BOOTLOADER_BlankCheckRegister(BOOTLOADER_CALLBACK newFunc)
{
    bootloaderData.BlankCheckFunc = newFunc;
}

void BOOTLOADER_ProgramCompleteRegister(BOOTLOADER_CALLBACK newFunc)
{
    bootloaderData.ProgramCompleteFunc = newFunc;
}

void BOOTLOADER_ForceBootloadRegister(BOOTLOADER_CALLBACK newFunc)
{
    bootloaderData.ForceBootloadFunc = newFunc;
}

// *****************************************************************************
/* Function:
    void Bootloader_Tasks (SYS_MODULE_INDEX index);

  Summary:
    Maintains the Bootloader module state machine. It manages the Bootloader Module object list
    items and responds to Bootloader Module primitive events.

*/
void Bootloader_Tasks ()
{
    size_t BuffLen=0;
    uint16_t crc;
    char longFileName[300];
    char tbuff[350];
    SYS_FS_HANDLE dirHandle;
    SYS_FS_FSTAT stat;
    static int dots = 0;
    unsigned int i;
    SYS_FS_RESULT SR;
    SYS_FS_FSTAT FileStat;
    static int BootloadMedia = Media_None;
    DRV_SST26_COMMAND_STATUS cmdStatus;
    static unsigned long t, tt, pt,toWait;
    static unsigned long progress = 0;
    /* Check the application state*/
    switch (bootloaderData.currentState) {
	case BOOTLOADER_CHECK_FSDEV:
	    USB_HOST_BusEnable(0);
	    PrintUart("\r\nEasyStream bootloader version %s\r\n", BootVersion);
	    bootloaderData.currentState = BOOTLOADER_CHECK_FOR_TRIGGER;
	    toWait = SYS_TMR_TickCounterFrequencyGet() / 4 * 3;// 3/4 seconds
	    onBootProgress(progress++);
	    break;
	case BOOTLOADER_CHECK_FOR_TRIGGER:
	{
	    int forceBootloadMode = false;
	    if (!USB_HOST_BusIsEnabled(0)) {
		t = SYS_TMR_TickCountGet();
		return;
	    }
	    pt = SYS_TMR_TickCountGet() - t;
	    if (pt < toWait) {
		progress = 100 * pt / toWait;
		onBootProgress(progress++);
		return;
	    } else {
		onBootProgress(false);
	    }
	    if (bootloaderData.ResetCause == Reset_WDT) {
		bootloaderData.currentState = BOOTLOADER_WDT;
		PrintUart("Watchdog reset\r\n");
	    } else if (!BL_ApplicationIsValid() || USBOTGbits.FSDEV) {
		//First time, we have to use USB
		forceBootloadMode = 1;
		BootloadMedia = Media_USB;
		Bootloader_SetDisk(Media_USB);
		if (!BL_ApplicationIsValid()) {
		    PrintUart("Application invalid\r\n");
		    onBootNotify("No firmware installed\rSearching for USB\rdrive");
		} else {
		    PrintUart("USB drive detected\r\n");
		    if (USBOTGbits.FSDEV) {
			onBootNotify("USB device detected\rOpening USB\rdrive");
		    } else {
			onBootNotify("No USB device found\rSearching for USB\rdrive");
		    }		    
		}		
		PrintUart("Searching for USB drive\r\n");
	    } else if (bootloaderData.ResetCause == Reset_SWR) {
		forceBootloadMode = 1;
		BootloadMedia = Media_Flash;
		Bootloader_SetDisk(Media_Flash);
		//PrintUart("EasyStream bootloader version %s\r\n", BootVersion);
		PrintUart("Opening Flash\r\n");
		onBootNotify("Opening flash");
	    } else {
		PrintUart("Booting to main application\r\n");
	    }
	    if (forceBootloadMode)
            {
                /* Override any soft reset from the bootloader, so we will do
                 one when bootloader mode is done. */
                bootloaderData.softReset = false;
                bootloaderData.currentState = BOOTLOADER_OPEN_DATASTREAM;
            }
            else
            {
                bootloaderData.currentState = BOOTLOADER_CLOSE_DATASTREAM;
            }
            break;
        }

	case BOOTLOADER_OPEN_DATASTREAM:
	{
	    if (DRV_SST26_Status(sysObj.drvSst26Obj0) == SYS_STATUS_READY) {

		bootloaderData.streamHandle = DATASTREAM_Open(DRV_IO_INTENT_READWRITE | DRV_IO_INTENT_NONBLOCKING);

		if (bootloaderData.streamHandle != DRV_HANDLE_INVALID) {
		    bootloaderData.currentState = BOOTLOADER_WAIT_FOR_HOST_ENABLE;
		} else {
		    PrintUart("Unable to open Flash handle\r\n");
		    bootloaderData.currentState = BOOTLOADER_ERROR;
		}
	    }
	    break;
	}

        case BOOTLOADER_WAIT_FOR_HOST_ENABLE:

            /* Check if the host operation has been enabled */
            if(DATASTREAM_ClientStatus(bootloaderData.streamHandle) == DRV_CLIENT_STATUS_READY)
            {
                /* This means host operation is enabled. We can
                 * move on to the next state */
                 DATASTREAM_BufferEventHandlerSet((DRV_HANDLE)bootloaderData.streamHandle, NULL, 0);
                 bootloaderData.currentState = BOOTLOADER_WAIT_FOR_DEVICE_ATTACH;
            }
	    t = tt = SYS_TMR_TickCountGet();
            break;

	case BOOTLOADER_WAIT_FOR_DEVICE_ATTACH:
	    /* Wait for device attach. The state machine will move
		     * to the next state when the attach event
		     * is received.  */
	    if (BootloadMedia == Media_Flash) {
		bootloaderData.currentState = BOOTLOADER_DEVICE_CONNECTED;
	    } else {
		if (SYS_TMR_TickCountGet() - tt > SYS_TMR_TickCounterFrequencyGet()) {
		    tt = SYS_TMR_TickCountGet();
		    PrintUart(".");
		    memset(tbuff, 0, sizeof (tbuff));
		    memset(tbuff, '.', dots);
		    if (dots<sizeof (tbuff) - 1) {
			dots++;
		    }
		    if (USBOTGbits.FSDEV) {
			onBootNotify("USB device detected\rOpening USB\rdrive%s", tbuff);
		    } else {
			onBootNotify("No USB device found\rSearching for USB\rdrive%s", tbuff);
		    }
		}
		if (SYS_TMR_TickCountGet() - t > SYS_TMR_TickCounterFrequencyGet()*30) {
		    PrintUart("No USB drive found\r\nSearching flash for firmware\r\n");
		    onBootNotify("No USB drive\found\rSearching flash");
		    Bootloader_SetDisk(Media_Flash);
		    BootloadMedia = Media_Flash;
		    bootloaderData.currentState = BOOTLOADER_DEVICE_CONNECTED;
		    //while(UartTxInProgress());
		    //SYS_RESET_SoftwareReset();
		}
	    }
	    break;

#if(SYS_FS_MAX_FILES > 0)
        case BOOTLOADER_DEVICE_CONNECTED:
            /* Turn on LED to indicate connection. */
            //BSP_LEDOn(BSP_LED_2);
            /* Device was connected. We can try opening the file */
            bootloaderData.currentState = BOOTLOADER_OPEN_FILE;
	    break;

        case BOOTLOADER_OPEN_FILE:

		    /* Try opening the file for reading */
	    if (BootloadMedia == Media_USB) {
		stat.lfname = longFileName;
		stat.lfsize = 300;
		dirHandle = SYS_FS_DirOpen("/mnt/myUsbDrive");
		if (dirHandle != SYS_FS_HANDLE_INVALID) {
		    if (SYS_FS_DirSearch(dirHandle, BootLoadSearchName, SYS_FS_ATTR_ARC, &stat) == SYS_FS_RES_FAILURE) {
			// Specified file not found
			PrintUart("No firmware files found in directory\r\n");
			onBootNotify("No firmware found\rin USB drive");
		    } else {
			strcpy(tbuff, "/mnt/myUsbDrive/");
			strcat(tbuff, stat.lfname);
			// File found. Read the complete file name from "stat.lfname" and
			// other file parameters from the "stat" structure
			bootloaderData.fileHandle = SYS_FS_FileOpen(tbuff, (SYS_FS_FILE_OPEN_READ));
		    }
		    SYS_FS_DirClose(dirHandle);
		}
		bootloaderData.fileHandle = SYS_FS_FileOpen(tbuff, (SYS_FS_FILE_OPEN_READ));
		if (bootloaderData.fileHandle == SYS_FS_HANDLE_INVALID) {
		    PrintUart("Unable to open firmware file %s\r\n", tbuff);
		    onBootNotify("Unable to open\rfirmware file");
		    bootloaderData.currentState = BOOTLOADER_ERROR;
		} else {
		    PrintUart("Firmware file %s opened\r\n", tbuff);
		    onBootNotify("Firmware file\r%s\ropened", tbuff);
		}
	    } else if (BootloadMedia == Media_Flash) {
		bootloaderData.fileHandle = SYS_FS_HANDLE_INVALID;
	    } else {
		bootloaderData.currentState = BOOTLOADER_ERROR;
	    }

            if(BootloadMedia == Media_USB && bootloaderData.fileHandle == SYS_FS_HANDLE_INVALID)
            {
		PrintUart("Unable to find firmware file\r\n");
		onBootNotify("Unable to find\rfirmware file");
		/* Could not open the file. Error out*/
		bootloaderData.currentState = BOOTLOADER_ERROR;
	    } else {
		/* File opened successfully. Read file */
		bootloaderData.currentState = BOOTLOADER_PREVALIDATE_FILE;
	    }
	    break;

	case BOOTLOADER_PREVALIDATE_FILE:
	    FileStat.lfname = longfilename;
	    FileStat.lfsize = 300;
	    if (BootloadMedia == Media_USB) {
		SR = SYS_FS_FileStat(tbuff, &FileStat);
	    } else if (BootloadMedia == Media_Flash) {
		SR = SYS_FS_RES_SUCCESS;
		FileStat.fsize = sizeof (_File);
	    } else {
		bootloaderData.currentState = BOOTLOADER_ERROR;
		break;
	    }
	    if (SR == SYS_FS_RES_FAILURE ) {
		onBootNotify("Unable to view\rfile size");
		PrintUart("Unable to view file size\r\n");
		bootloaderData.currentState = BOOTLOADER_ERROR;
		break;
	    } else if (FileStat.fsize != sizeof (_File)) {
		onBootNotify("firmware file size\rincorrect");
		PrintUart("Incorrect image size\r\n");
		bootloaderData.currentState = BOOTLOADER_ERROR;
		break;
	    } else {
		longfilename[30] = 0; //Null terminate safety
		onBootNotify("File size\r%i bytes", FileStat.fsize);
		PrintUart("File size %i\r\n", FileStat.fsize);
	    }
	    Bootloader_Init();
	    bootloaderData.currentState = BOOTLOADER_VALIDATE_FILE;
	    break;
	    
	case BOOTLOADER_VALIDATE_FILE:
	    i = Bootloader_ProcessFile(1);
	    switch(i){
		case BLT_INVALID:
		    onBootNotify("Firmware file\rerror");
		    PrintUart("Image did not validate\r\n");
		    bootloaderData.currentState = BOOTLOADER_ERROR;
		    break;
		case BLT_SUCCESS:
		    onBootNotify("Firmware file\rvalid\rerasing memory");
		    PrintUart("Image validated, erasing memory\r\n");
		    bootloaderData.currentState = BOOTLOADER_ERASE_EXTERNAL;
		    Bootloader_Init();

		    PrintUart("Erasing external flash\r\n");
		    DRV_SST26_Erase(bootloaderData.streamHandle, &bootloaderData.commandHandle,
			    FLASH_USER / 0x1000, FLASH_USER_LENGTH / 0x1000);
		    if (DRV_SST26_COMMAND_HANDLE_INVALID == bootloaderData.commandHandle) {
			PrintUart("Unable to erase external flash\r\n");
			bootloaderData.currentState = BOOTLOADER_ERROR;
			return;
		    }
		    break;
		default:
		    break;
	    }
	    break;

	case BOOTLOADER_ERASE_EXTERNAL:
	    cmdStatus = DRV_SST26_CommandStatus(bootloaderData.streamHandle, bootloaderData.commandHandle);
	    if (cmdStatus == DRV_SST26_COMMAND_COMPLETED) {
		onBootNotify("External flash\rerased\rerasing internal");
		PrintUart("External Flash erased\r\n");
		bootloaderData.currentState = BOOTLOADER_ERASE_NVM;
	    } else if (DRV_SST26_COMMAND_ERROR_UNKNOWN == cmdStatus) {
		PrintUart("Unable to fully erase external flash\r\n");
		bootloaderData.currentState = BOOTLOADER_ERROR;
	    }
	    break;
	    
	case BOOTLOADER_ERASE_NVM:
	    PrintUart("Erasing internal flash\r\n");
	    APP_FlashErase();
	    while (!PLIB_NVM_FlashWriteCycleHasCompleted(NVM_ID_0));
	    APP_NVMClearError();
	    PLIB_NVM_MemoryModifyInhibit(NVM_ID_0);
	    bootloaderData.currentState = BOOTLOADER_READ_FILE;
	    onBootNotify("Internal flash\rerased");
	    break;

	case BOOTLOADER_READ_FILE:

	    i = Bootloader_ProcessFile(0);
	    switch (i) {
		case BLT_UNFINISHED:
		    break;
		case BLT_INVALID:
		    onBootNotify("Firmware program\rerror");
		    PrintUart("Image did not write correctly\r\n");
		    bootloaderData.currentState = BOOTLOADER_ERROR;
		    break;
		case BLT_SUCCESS:
		    onBootNotify("Firmware program\rsuccess\rBooting to main\rprogram");
		    PrintUart("Image written to memory\r\n");
		    PrintUart("Booting to main application\r\n");
		    bootloaderData.currentState = BOOTLOADER_CLOSE_DATASTREAM;
		    break;
		default:
		    break;
	    }

	    break;
#endif
        case BOOTLOADER_CLOSE_DATASTREAM:
	    onBootClose();
	    //ClearLeds;
            DATASTREAM_Close();
	    if (bootloaderData.ProgramCompleteFunc != NULL) {
		bootloaderData.ProgramCompleteFunc();
	    }
	    while (UartTxInProgress());
	case BOOTLOADER_ENTER_APPLICATION:
	    Enter_Application();

            break;

        case BOOTLOADER_ERROR:
            /* The application comes here when the demo
             * has failed. Switch on the LED 9.*/
            //BSP_LEDOn(BSP_LED_3);
            //BSP_LEDOn(BSP_LED_2);
            //BSP_LEDOn(BSP_LED_1);
	    if(BL_ApplicationIsValid()){
		bootloaderData.currentState = BOOTLOADER_CLOSE_DATASTREAM;
	    }
	    onBootError();
	    //Led_Status_Red_ON;
	    //Led_Play_Red_ON;
            break;
	case BOOTLOADER_WDT:
	    if (BL_ApplicationIsValid()) {
		bootloaderData.currentState = BOOTLOADER_CLOSE_DATASTREAM;
	    }
	    onBootWDT();
	    break;
	default:
            bootloaderData.currentState = BOOTLOADER_ERROR;
            break;
    }

#if((DRV_USBFS_HOST_SUPPORT == false) && \
    (DRV_USBHS_HOST_SUPPORT == false) && \
    !defined(DRV_SDCARD_INSTANCES_NUMBER) && \
    !defined(DRV_SDHC_INSTANCES_NUMBER))
    /* Maintain Device Drivers */
    DATASTREAM_Tasks();
    #endif

}

void BLMedia_EraseKey(void)
{
    unsigned int nvmResult;
    void *pTarget = (void *)&g_AppVerify;
    //DBGPRINT(("Erase Key Before at %p: AppAddr=%08lx Key=%08lx FirstLong=%08lx\n", pTarget, g_AppVerify.ApplicationAddress, g_AppVerify.ValidKey, g_AppVerify.FirstLong));
    APP_PageErase(pTarget);
    //DBGPRINT(("After: AppAddr=%08lx Key=%08lx FirstLong=%08lx\n", g_AppVerify.ApplicationAddress, g_AppVerify.ValidKey, g_AppVerify.FirstLong));
}

void BLMedia_SaveKey(void) {
    APP_NVMWordWrite((void *) &g_AppVerify.ApplicationAddress, APP_FLASH_BASE_ADDRESS);
    APP_NVMWordWrite((void *) &g_AppVerify.ValidKey, APPLICATION_VALID_KEY);
    APP_NVMWordWrite((void *) &g_AppVerify.FirstLong, ((long *) APP_FLASH_BASE_ADDRESS)[0]);
    APP_NVMWordWrite((void *) &g_AppVerify.SecondLong, ((long *) APP_FLASH_BASE_ADDRESS)[1]);
    Nop();
}
